/*
Ce fichier contient l’ensemble des fonctions permettant de faire les requêtes aux
différentes apis et d’afficher les résultats obtenus.
*/

//LES FONCTIONS
function calcul(pathIGN,pathGG,pathOSM){
  /*
  Cette fonction permet d’obtenir les résultats des différentes Api de construire les traces.
  Param pathIGN : il s’agit de la liste des points saisies formatée en chaine de caractère
  pour la requête à l’api de l’IGN. C’est à dire : « lon= » liste des longitudes séparées
  par « | » , « & lat= » liste des latitudes séparées par « | ».
  Param pathGG : il s’agit de la liste des points saisies formatée en chaine de caractère
  pour la requête à l’api de Google. C’est-à-dire :  une liste de point séparées par ce
  caractères « | » où chaque point est donné sous forme : latitude «,» longitude.
  Param pathOSM : il s’agit de la liste des points saisies formatée en chaine de caractère
  pour la requête à l’api de OpenRoute. C’est-à-dire une liste de point séparées par ce
  caractère « | » où chaque point est donné sous forme : longitude «,» latitude.
  Return : traceIGN, traceGG, traceOSM. Il s’agit de la trace de la courbe à placer
  sur le graphique. Elle doit prendre la forme d’un objet js avec comme propriété x :
  le tableau avec toutes les abscisses des points ; y : le tableau avec toutes les ordonnées
  de points ; type : 'scatter' ; et éventuellement un nom qui servira à construire la légende.
  */
  var traceGG=null;
  var traceIGN=null;
  var traceOSM=null;
  var nn=document.getElementById("nombre").value;
  verif=[0,0,0];

  //requete ajax vers l'ign
  var key="ysxcztwpwi01twxexn5lomnw";
  var url ="https://wxs.ign.fr/"+key+"/alti/rest/elevationLine.json?"+pathIGN+"&zonly=true&sampling="+nn;
  var ajaxIG = new XMLHttpRequest();
  ajaxIG.open('GET',url, true);
  ajaxIG.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajaxIG.addEventListener('readystatechange',  function (e)  {
    if(ajaxIG.readyState == 4 && ajaxIG.status == 200) {
      res=JSON.parse(ajaxIG.responseText).elevations;
      var x=[];
      var y=[];
      //calcul des coordonées des points sur le grpah
      for (var i =0; i<res.length;i++){
        if (i==0){
          x.push(i);
        }else{
          //l'abcisse est celle du point precedent plus la distance entre le point et son predecesseur.
          x.push(precisionRound(x[i-1]+distance(Number(res[i]['lat']),Number(res[i]['lon']),Number(res[i-1]['lat']),Number(res[i-1]['lon'])),5));
        }
        //l'ordonnnee est l'altitude du point.
        y.push(precisionRound(res[i]['z'],2));
      }
      //creation de la trace
      traceIGN = {
        x: x,
        y: y,
        type: 'scatter',
        name: 'IGN'
      };
      //confirmation de bonne execution de la requete
      verif[0]=1;
      //verification de l'execution de toutes les requetes
      if (verif[1]==1&&verif[0]==1&&verif[2]==1){
        //écriture du graph
        plot(traceIGN,traceGG,traceOSM);
        verif=[0,0,0];
      }
  }});
  ajaxIG.send();

  //requete ajax vers le php qui recupere le resultat de la requete google (proxy pour eviter les probleme de cross domain)
  var ajaxG = new XMLHttpRequest();
  ajaxG.open('GET',"js/phpGG.php?path="+pathGG+"&n="+nn, true);
  ajaxG.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajaxG.addEventListener('readystatechange',  function (e)  {
     if(ajaxG.readyState == 4 && ajaxG.status == 200) {
       var resGG=JSON.parse(ajaxG.responseText).results;
       var x=[];
       var y=[];
       //calcule des coordonnnées des points sur le graph
       for (var i=0; i<resGG.length;i++){
         if (i==0){
           x.push(i);
         }else{
           x.push(precisionRound(x[i-1]+distance(Number(resGG[i]['location']['lat']),Number(resGG[i]['location']['lng']),Number(resGG[i-1]['location']['lat']),Number(resGG[i-1]['location']['lng'])),5));
         }
         y.push(precisionRound(resGG[i]['elevation'],2));
       }
       //creation de la trace
       traceGG = {
         x: x,
         y: y,
         type: 'scatter',
         name: 'Google'
       }
     }
     //confirmation d'execution
     verif[1]=1;
     //verification de l'execution de toutes les requetes
     if (verif[0]==1&&verif[1]==1&&verif[2]==1){
       //ecriture du graph
       plot(traceIGN,traceGG,traceOSM);
       verif=[0,0,0];
     }
  });
  ajaxG.send();

  var key="58d904a497c67e00015b45fc8aab4aa30d9a44a18d5d0feda3c0e03f";
  //requete ajax vers openroute openrouteservice
  var urlOSM="https://api.openrouteservice.org/directions?api_key="+key+"&coordinates="+pathOSM+"&elevation=true&profile=foot-walking&instructions=false&format=json&language=fr&geometry_format=geojson&units=km";
  var ajaxOSM = new XMLHttpRequest() ;
  ajaxOSM.open('GET', urlOSM);
  ajaxOSM.addEventListener('readystatechange',  function(e) {
    if(ajaxOSM.readyState == 4 && ajaxOSM.status == 200) {
      var resOSM=JSON.parse(ajaxOSM.responseText);
      var resOSM2=resOSM['routes'][0]['geometry']['coordinates'];
      //calcul des points a conserver 1 tous les n pour en avoir nn
      var n =precisionRound(resOSM2.length/nn,0);
      var x=[];
      var y=[];
      //calcul de la position des points sur le graph
      for (var i =0;i<nn;i++){
        if (i==0){
          x.push(i);
        }else{
          x.push(precisionRound(x[i-1]+distance(Number(resOSM2[i*n][0]),Number(resOSM2[i*n][1]),Number(resOSM2[(i-1)*n][0]),Number(resOSM2[(i-1)*n][1])),5));
        }
        y.push(precisionRound(resOSM2[i*n][2],2));
      }
      //creaton de la trace
      traceOSM={
        x: x,
        y: y,
        type: 'scatter',
        name: 'OSM'
      }
    }
    //confirmation de fin de la requete
    verif[2]=1;
    //verification d'execution de toutes les requetes
    if (verif[0]==1&&verif[1]==1&&verif[2]==1){
      //affichage du graph
      plot(traceIGN,traceGG,traceOSM);
      verif=[0,0,0];
    }
  });
  ajaxOSM.send();
}

//fonction d'affichage du graph
function plot(trace1,trace2,trace3){
  /*
  Cette fonction permet d’afficher le graphique.
  Param traceI : Il s’agit de la trace de la I eme courbe à placer sur le graphique.
  Elle doit prendre la forme d’un objet js avec comme propriété x: le tableau avec
  toutes les abscisses des points ;  y:  le tableau avec toutes les ordonnées de points ;
  type: 'scatter' ; et éventuellement un nom qui servira à construire la légende. I appartient à {1,2,3}.
  */
  Plotly.newPlot('graph', [trace1,trace2,trace3]);
}

function distance(x1,y1,x2,y2){
  /*
  Cette fonction calcule la distance entre deux points.
  Param x1 : il s’agit d’un float représentant la latitude du premier point.
  Param y1 : il s’agit d’un float représentant la longitude du premier point.
  Param x2 : il s’agit d’un float représentant la latitude du second point.
  Param y2 : il s’agit d’un float représentant la longitude du second point.
  Return : la distance euclidienne entre les points sans considérer le relief.
  */
  return(Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
}

function precisionRound(number, precision) {
  /*
  Cette fonction arrondi un nombre au nombre de décimale voulu.
  Param number : il s’agit du nombre que l’on souhaite arrondir.
  Param precision : il s’agit du nombre de décimale voulu ; ce nombre doit être un entier.
  Return : le nombre arrondi.
  */
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}
