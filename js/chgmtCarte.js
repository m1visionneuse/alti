/*
Ce fichier contient l’ensemble des fonctions relatives à la carte y compris
la saisie des points et leur traitement en chaine de caractères aux format attendu
par les apis.
*/

//LES VARIABLES GLOBALES
var markersObject = {};
var tab =[];
var i=0;
var verif=[0,0,0];

//LES EVENEMENTS
document.getElementById("eff").addEventListener("click",effacement);
document.getElementById("select").addEventListener('click',afficher) ;
document.getElementById("button").addEventListener("click",traitement);

//LES FONCTIONS
function afficher(){
  /*
  Cette fonction permet de détecter l’option sélectionné dans le menu choix de carte
  et d’afficher la carte correspondante.
  */
  i=0;
  //detection de la carte voulue
  if(document.getElementById('select').value == "carte_fond"){
    afficher_scan();
  }else{
    afficher_ortho();
  }
}


function effacement (){
  /*
  Cette fonction permet d’effacer les points saisis du tableau tab ; d’effacer
  le graphique et de réinitialiser la carte.
  */
  i=0;
  afficher();
  document.getElementById("graph").innerHTML="";
  tab=[];
}

function afficher_scan(){
  /*
  Cette fonction affiche le fond carte standard de la bd topo de l’IGN
  */
  tab=[];
    document.getElementById('map').innerHTML = " ";
    map.remove();
    window.map  = L.map('map', {
      zoom : 14,
      center : L.latLng(47.3685,3.279589)
    });
    L.tileLayer(
       'https://wxs.ign.fr/ysxcztwpwi01twxexn5lomnw/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix={z}&tilecol={x}&tilerow={y}&layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&format=image/jpeg&style=normal',
       {
           minZoom : 0,
           maxZoom : 18,
       }).addTo(map);
  map.on('click', placeMarker);
  var markersObject = {};
 }

function afficher_ortho(){
  /*
  Cette fonction affiche le fond vue satellite de la bd ortho de l’IGN
  */
    tab=[];
    document.getElementById('map').innerHTML = "";
    map.remove();
    // ajout de la carte
    window.map  = L.map('map', {
       zoom : 14,
       center : L.latLng(47.3685,3.279589)
    });
    // Création de la couche
    L.tileLayer(
       'https://wxs.ign.fr/ysxcztwpwi01twxexn5lomnw/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix={z}&tilecol={x}&tilerow={y}&layer=ORTHOIMAGERY.ORTHOPHOTOS&format=image/jpeg&style=normal',
       {
         minZoom : 0,
         maxZoom : 18,
     }).addTo(map);
     map.on('click', placeMarker);
}

function traitement(){
  /*
  Cette fonction récupère le tableau constitué des positions de l’ensemble des clics
  de l’utilisateur et crée en trois chaines de caractères formatées chacune pour la
  requête d’une api.
  Param tab : il s’agit du tableau des positions saisies de la forme [[lat1, lng1], …, [latn, lngn]]
  Return : pathIGN, pathGG, pathOSM. Il s’agit de la liste des points saisies formatée
  en chaine de caractère pour la requête à l’api correspondante.
  */
    var lonIGN="lon=";
    var latIGN="lat=";
    var pathGG="";
    var pathOSM="";
    for (var i=0; i<tab.length;i++){
      if (i==tab.length-1){
        lonIGN+=tab[i][1];
        latIGN+=tab[i][0];
        pathGG+=tab[i][0]+","+tab[i][1];
        pathOSM+=tab[i][1]+","+tab[i][0];
      }else{
        lonIGN+=tab[i][1]+"|";
        latIGN+=tab[i][0]+"|";
        pathGG+=tab[i][0]+","+tab[i][1]+"|";
        pathOSM+=tab[i][1]+","+tab[i][0]+"|";
      }
    }
    pathIGN=lonIGN+"&"+latIGN;
    calcul(pathIGN,pathGG,pathOSM);
}

function placeMarker(e) {
  /* Cette fonction place un marqueur sur la carte lors du clic de l’utilisateur
  et ajoute le point dans le tableau de saisie. Il gère aussi le déplacement des
  marqueurs et actualise le tableau de saisie pour qu’il corresponde toujours aux
  marqueurs.
  Param e : évènement clic sur la carte on utilise surtout sa propriété _LatLng.
  */
  lat = e.latlng.lat;
  lng = e.latlng.lng;
  markersObject[i]= L.marker([lat,lng],{
    draggable : true,
    id:i
  });
  tab.push([lat,lng]);
  markersObject[i].on('dragend',function(e){tab[e.target.options.id]=[e.target._latlng.lat,e.target._latlng.lng];});
  markersObject[i].addTo(map);
  i=i+1;
}
